import telepot
from telepot.loop import MessageLoop
import time
import json
import iota


"Read configuration info from file: Bot token, node_name and password"
while True:
    try:
        with open('/home/IotaSpammerBot/config.txt') as configFile:
            configfileData = json.load(configFile)
        print('Opening file...')
        TOKEN = configfileData['bot_token']
        admin_list = configfileData['admin_list']
        print("Initialization completed!")
        break
    except:
        print("'config.txt' file not found...\n")
        print("Please create the file and place it in script folder...\n")
        input("After file creation, press enter to retry...\n\n");

"Initialization: second step"
spammerIsActive = 0
myBot = telepot.Bot(TOKEN)
rx_address = 'IOTA9SPAMMER9BOT9TEST999999999999999999999999999999999999999999999999999999999999'
nodeURL = 'https://iota-man2.com:14267'
api = iota.Iota(nodeURL)
iteration = 0

"Testing the bot"
print(myBot.getMe())


"Define the message listener function"
def messageListener(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    user_name = msg['chat']['username']
    user_id = msg['chat']['id']
    global spammerIsActive
    if (content_type == 'text') & (any(user_name in x for x in admin_list)):
        if msg['text'] == '/start':
            myBot.sendMessage(user_id, 'Ready to SPAM!')
        if msg['text'] == '/stop':
            spammerIsActive = 0
            myBot.sendMessage(user_id, 'Bot has been stopped!')
        if msg['text'] == '/spam':
            spammerIsActive = 1
            myBot.sendMessage(user_id, 'I\'m SPAMMING this node: ' + nodeURL)
            time.sleep(1)
    else:
        myBot.sendMessage(user_id, 'Your message is not a correct command or your USERNAME is not allowed to send commands')
        myBot.sendMessage(user_id, 'Please contact the BOT administrator')

"Function to generate a transaction"
def promoteTX(rx_address):

    ### TX CREATION ##
    print("\n\n     ### TX CREATION ##")
    tx_message = "IOTA ITALIA RULEZ"
    tx_tag = iota.Tag(b'IOTA9FULL9NODE9ITA')
    tx = iota.ProposedTransaction(address = iota.Address(rx_address),
                                  message = iota.TryteString.from_unicode(tx_message),
                                  tag = tx_tag,
                                  value = 0)
    print('     Created first transaction: ')
    print("     " + str(vars(tx)))

    ### BUNDLE FINALIZATION ###
    print("\n\n     ### BUNDLE FINALIZATION ###")
    bundle = iota.ProposedBundle(transactions = [tx])
    bundle.finalize()
    print("     Bundle is finalized...")
    print("     Generated bundle hash: %s" % (bundle.hash))
    print("     List of all transaction in the Bundle:\n")
    for txn in bundle:
        print("     " + str(vars(txn)))
    bundle_trytes = bundle.as_tryte_strings() # bundle as trytes

    ### TIP SELECTION ###
    print("\n\n     ### TIP SELECTION ###")
    tips = api.get_transactions_to_approve(depth = 3)
    print("     " + str(tips))

    ### POW ###
    print("\n\n     ### POW ###")
    try:
        attached_tx = api.attach_to_tangle(trunk_transaction=tips['trunkTransaction'], branch_transaction=tips['branchTransaction'], trytes=bundle_trytes, min_weight_magnitude=14)
    except:
        print('Can\'t execute POW and attach the TX to Tangle')

    ### BROADCASTING ###
    print("     Broadcasting transaction...")
    res = api.broadcast_and_store(attached_tx['trytes'])
    print("     " + str(res))

    ### TRANSACTION RECAP ###
    print("\n\n     ### TRANSACTION RECAP ###")
    print("     " + str(vars(attached_tx['trytes'][0])))

    sent_tx = iota.Transaction.from_tryte_string(attached_tx['trytes'][0])
    print("     Transaction Hash: " + str(sent_tx.hash))
    return ("")


"Activate listening loop"
MessageLoop(myBot, messageListener).run_as_thread()
print('Listening ...')

"Generate transaction if the bot is active"
while 1:
    if (spammerIsActive == 1):
        try:
            tx_hash_to_be_promoted = promoteTX(rx_address)
            iteration = iteration + 1
        except:
            print('Can\'t generate transaction')

"Keep the program running."
while 1:
    time.sleep(1)

